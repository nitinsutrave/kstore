# KStore

This is an application built using [Electron](https://electronjs.org/) which you can use to store data in an easy access fashion

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/nitinsutrave/kstore.git
# Go into the repository
cd kstore
# Install dependencies
npm install
# Run the app
npm start
```

## To Build

To build a dmg for use on your mac, run the following on the command line:

```bash
npm run build
```
