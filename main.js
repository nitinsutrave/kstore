'use strict'

const _ = require('lodash')
const { app, BrowserWindow, Menu } = require('electron')
const { ipcMain } = require('electron')
const fs = require('fs')
var crypto = require('crypto');

function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 960,
    height: 720,
    frame: false
  })

  // and load the index.html of the app.
  win.loadFile('renderers/main.html')

  // Open the DevTools.
  // win.webContents.openDevTools()

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })

  const template = [
    {
      label: "Application",
      submenu: [
          { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
          { type: "separator" },
          { label: "Quit", accelerator: "Command+Q", click: function() { app.quit(); }}
      ]
    },
    {
      label: "Edit",
      submenu: [
          { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
          { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
          { type: "separator" },
          { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
          { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
          { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
          { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
      ]
    }
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

// Set the app's name
app.setName('KStore')

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

const encryptData = (plainText) => {
  const mykey = crypto.createCipher('aes-128-cbc', 'mypassword');
  let mystr = mykey.update(plainText, 'utf8', 'hex')
  mystr += mykey.final('hex');
  return mystr
}

const decryptData = (cipherText) => {
  const mykey = crypto.createDecipher('aes-128-cbc', 'mypassword');
  let mystr = mykey.update(cipherText, 'hex', 'utf8')
  mystr += mykey.final('utf8');
  return mystr
}

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipcMain.on('add-new-kv', (event, key, value) => {
  const dataFilePath = app.getPath('userData') +  '/kvdata.csv'

  // read the existing file's data
  const cipherText = fs.readFileSync(dataFilePath, { encoding: 'utf8', flag: 'as+' })
  let rawData = cipherText
  if (cipherText) {
    rawData = decryptData(cipherText)
  }

  // parse the data into an array of key value pairs
  let kvArray = rawData.split('\n').map((line) => line.split(',')).filter((row) => !!row[0])

  const presentKeys = _.map(kvArray, '0')

  if (_.findIndex(presentKeys, (e) => e===key) >= 0) {
    event.returnValue = { isErr: true, errMsg: "Key already exists!" }
    return
  }

  // push the newly inputted key value
  kvArray.push([key, value])

  // sort alphabetically by key
  kvArray = _.sortBy(kvArray, [(kv) => kv[0]])

  // store the key-values back to file
  const parsedKvs = kvArray.map((pair) => pair.join(',')).join('\n')
  fs.writeFileSync(dataFilePath, encryptData(parsedKvs))
  
  event.returnValue = { value: kvArray }
})

ipcMain.on('dom-created', (event) => {
  const dataFilePath = app.getPath('userData') +  '/kvdata.csv'

  // read the existing file's data
  const cipherText = fs.readFileSync(dataFilePath, { encoding: 'utf8', flag: 'as+' })
  let rawData = cipherText
  if (cipherText) {
    rawData = decryptData(cipherText)
  }

  // parse the data into an array of key value pairs
  let kvArray = rawData.split('\n').map((line) => line.split(',')).filter((row) => !!row[0])

  // sort alphabetically by key
  kvArray = _.sortBy(kvArray, [(kv) => kv[0]])

  event.returnValue = kvArray
})

ipcMain.on('delete-kv', (event, key, value) => {
  const dataFilePath = app.getPath('userData') +  '/kvdata.csv'

  // read the existing file's data
  const cipherText = fs.readFileSync(dataFilePath, { encoding: 'utf8', flag: 'as+' })
  const rawData = decryptData(cipherText)

  // parse the data into an array of key value pairs
  let kvArray = rawData.split('\n').map((line) => line.split(',')).filter((row) => !!row[0])

  const presentKeys = _.map(kvArray, '0')

  const keyIndex = _.findIndex(presentKeys, (e) => e===key)
  if (keyIndex < 0) {
    event.returnValue = { isErr: true, errMsg: "Whoops! Something weird just happened!" }
    return
  }

  // delete the key
  _.pullAt(kvArray, [keyIndex]);

  // sort alphabetically by key
  kvArray = _.sortBy(kvArray, [(kv) => kv[0]])

  // store the key-values back to file
  const parsedKvs = kvArray.map((pair) => pair.join(',')).join('\n')
  fs.writeFileSync(dataFilePath, encryptData(parsedKvs))
  
  event.returnValue = { value: kvArray }
})
