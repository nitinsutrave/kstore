'use strict'

const _ = require('lodash')
const {ipcRenderer}  = require('electron')

const {clipboard} = require('electron')

const renderTable = (kvArray) => {
  // generate new html for the table
  let html = ''
  _.forEach(kvArray, (kvPair) => {
    html = html + `<tr> \
      <td>${kvPair[0]}</td> \
      <td>${kvPair[1]}</td> \
      <td><button type="button" class="btn btn-info" onclick="copyToClipboard(this)">Copy</button></td> \
      <td><button type="button" class="btn btn-danger" onclick="deleteKey(this)">Delete</button></td> \
      </tr>`
  })

  // render new table
  const table = document.getElementById('dataTable')
  table.innerHTML = html
}

const copyToClipboard = (element) => {
  const data = element.parentElement.parentElement.children[1].innerText
  clipboard.writeText(data)
}

const deleteKey = (element) => {
  const isConfirmed = confirm("This cannot be undone! Are you sure you want to delete?");
  if(!isConfirmed) {
    return
  }

  const key = element.parentElement.parentElement.children[0].innerText

  const response = ipcRenderer.sendSync('delete-kv', key)

  if (response.isErr) {
    alert(response.errMsg)
    return
  }

  renderTable(response.value)
}

const addKV = (element) => {
  const keyField = element.parentElement.parentElement.children[0].firstElementChild
  const valField = element.parentElement.parentElement.children[1].firstElementChild

  if (!keyField.value || !valField.value) {
    alert('Both Key and Value are required')
    return
  }

  const response = ipcRenderer.sendSync('add-new-kv', keyField.value, valField.value)

  if (response.isErr) {
    alert(response.errMsg)
    return
  }

  renderTable(response.value)

  // reset the input fields
  keyField.value = ''
  valField.value = ''
}

const searchKeys = (element) => {
  const searchText = document.getElementById('searchBox')
  const filter = searchText.value.toUpperCase();

  const table = document.getElementById('dataTable');
  const rows = table.getElementsByTagName('tr');
  for (let i = 0; i < rows.length; i++) {
    const key = rows[i].getElementsByTagName('td')[0];
    if (key) {
      const keyValue = key.textContent || key.innerText

      if (keyValue.toUpperCase().indexOf(filter) > -1) {
        rows[i].style.display = ''
      } else {
        rows[i].style.display = 'none';
      }
    }       
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const kvArray = ipcRenderer.sendSync('dom-created')
  renderTable(kvArray)
}, false);
